# README #

### What is this repository for? ###

The python script country_useragents.py takes pcap files in a folder and prints out the following for each pcap file.

pcapfilename		mobile-IP	country	useragent

trace_20160802.pcap	119.63.142.34	PK	Mozilla/5.0 (Linux; U; Android 4.4.4; en-au; SM-G530FZ Build/KTU84P) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30

### Python dependencies ###
(a) pyshark
(b) python-geoip-geolite2

Edit path for folders containing pcap files in script country_useragents.py. Sample pcap file trace_20160802.pcap provided.

### How to run ###
$python country_useragents.py | tee out.txt

learnbasics.txt and newsbugle.txt are two sample out.txt files for our two services Learn Basics and Bugle News. learnbasics.txt has 416715 entries from 07.07.2016 to 30.09.2016. newbugle.txt has 289582 entries from 15.09.2016 to 15.12.2016.

learnbasics_useragents_all.txt contains the useragent column from learnbasics.txt. learnbasics_useragents_pak.txt contains the useragent column from learnbasics.txt, where the country column is PK (Pakistan). Similarly for newsbugle.

import pyshark 
import os
from os.path import isfile, join
from geoip import geolite2
folder = 'path to pcap files'
pcap_files=[]
pcap_files += [each for each in os.listdir(folder) if each.endswith('.pcap')]
for pcap in pcap_files:	
	filtered_cap = pyshark.FileCapture(pcap, display_filter='http.user_agent and http.x_forwarded_for')
	for x in filtered_cap:
		ip_addr = x['http'].x_forwarded_for.split(", ")[0]
		user_agent = x['http'].user_agent
		try:
			country = geolite2.lookup(ip_addr).country
			print str(pcap)+"\t"+str(ip_addr)+"\t"+str(country)+"\t"+str(user_agent)
		except:
			pass

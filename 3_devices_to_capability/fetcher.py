import urllib2
import sys

vendor=str(sys.argv[1])
file = open("links/"+vendor+"links.txt", 'r')

for line in file:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	req = urllib2.Request(separatedList[0])
	response = urllib2.urlopen(req)
	the_page = response.read()
	separatedList[1] = separatedList[1].strip('\n')
	pathname = vendor+"/" + separatedList[1] + ".html"
	newfile = open(pathname, 'w')
	newfile.write(the_page)
	newfile.close()

# README #

### What is this repository for? ###
For 34 vendors, mobile devices are mapped to their capabilities in this folder. Then devices extracted in [2_useragents_to_device](../2_useragents_to_devices) are matched to the vendor devices and their capabilities.

### Python dependency ###
fuzzywuzzy and python-Levenshtein
 
### File description ###
1. list contains the names of 34 mobile device vendors.

2. [links/](links) folder contains 34 text files, one for each vendor. These links/${vendor}links.txt files were created using https://magic.import.io/. This is a website that let us extract links from a page by detecting the pattern of items and their links.
We gave import.io any website that has further links (e.g. http://www.imei.info/phonedatabase/2-phones-alcatel/). import.io automatically crawled all pages under that page to get all the links, which we saved in the text file. The links/ folder has all the links that we already crawled, so the following analysis can be done with that.

3. fetcher.sh calls fetcher.py for each vendor in "list". fetcher.py takes each link in links/${vendor}links.txt and downloads the webpage corresponding to that link in a temporary folder. 

4. filler.sh calls filler.py for each vendor in "list". filler.py takes each webpage from the temporary folder created by fetcher.sh. The webpage contains details for a particular device model) and extracts specified capability features. Sample outputs are in the [device_capability/](device_capability) folder.

5. [device_matching/](device_matching) is the final step to match the mobile devices extracted in [2_useragents_to_device](../2_useragents_to_devices) to the models and capabilities extracted in [device_capability/](device_capability). It contains the following components:

 * For all files of the form ${vendor}phones.csv in [device_capability/](device_capability), there is a file ${vendor}.txt. This removes commas and the first description line from the csv. These files contain the names of devices from each vendor, along with capabilities.
 * For each devices_count.txt file in [2_useragents_to_device](../2_useragents_to_devices) sub-folders, there is a file here named the same as the original sub-folder: learnbasics-all.txt, learnbasics-pak.txt, newsbugle-all.txt, newsbugle-pak.txt. These files have "\t", instead of "___" separating the phone name and the count. phones_all.txt and phones_pak.txt are created combining all devices for the two services, using the following command (similar command for pak).
```shell
cat learnbasics-all.txt newsbugle-all.txt | awk -F"\t" '{a[$1]+=$2}END{for(i in a) print i"\t"a[i]}' | sort > phones_all.txt
``` 
 *  matcher.py matches phones_all.txt and phones_pak.txt to the vendor devices and capabilities, to produce output files mapped_all.txt  and mapped_pak.txt.
 
 * stats.sh takes phones_all.txt and phones_pak.txt and outputs percentage of the capabilities, which are used in plots.

### How to run ###
$./fetcher.sh

$./filler.sh

$cd device_matching

$python matcher.py

$./stats.sh

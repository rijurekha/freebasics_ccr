import sys
vendor=str(sys.argv[1])
support = ['<div class="tt">GSM:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">GPRS:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">EDGE:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">HSDPA:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">HSCSD:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">LTE:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">WAP:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">XHTML:</div><div class="cc"><img src="/gfx/ico-tak.png',
			'<div class="tt">SyncML:</div><div class="cc"><img src="/gfx/ico-tak.png']
nosupport = ['<div class="tt">GSM:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">GPRS:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">EDGE:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">HSDPA:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">HSCSD:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">LTE:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">WAP:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">XHTML:</div><div class="cc"><img src="/gfx/ico-nie.png',
			'<div class="tt">SyncML:</div><div class="cc"><img src="/gfx/ico-nie.png']
nowapsupport = '<div class="tt">WAP:</div><div class="cc"><img src="/gfx/ico-nie.png'
wapsupport = '<div class="tt">WAP:</div><div class="cc"><img src="/gfx/ico-tak.png'

writeFile = open(vendor+"phones.csv", 'w')
writeFile.write("\"Phone\",\"GSM\",\"GPRS\",\"EDGE\",\"HSDPA\",\"HSCSD\",\"LTE\",\"WAP\",\"XHTML\",\"SyncML\"\n")

file = open("links/"+vendor+"links.txt", 'r')

for line in file:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[1] = separatedList[1].strip('\r\n')

	phoneName = separatedList[1]
	pathname = vendor+"/" + phoneName + ".html"
	phoneFile = open(pathname, 'r')

	writeFile.write("\"" + phoneName + "\",")

	for x in support:
		found = False
		myFile = open(pathname, 'r')
		for line in myFile:
			if line.find(x) != -1:
				writeFile.write("\"Yes\",")
				found = True
				break
		if not(found):
			writeFile.write("\"No\",")
	writeFile.write("\n")

file.close()
writeFile.close()

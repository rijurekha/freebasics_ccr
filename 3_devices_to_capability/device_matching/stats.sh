#/bin/bash
countSyncML=`cat mapped_all.txt | awk -F"\t" '{if($(NF-1)=="Yes"){count=count+1}}END{print count}'`
countXHTML=`cat mapped_all.txt | awk -F"\t" '{if($(NF-2)=="Yes"){count=count+1}}END{print count}'`
countWAP=`cat mapped_all.txt | awk -F"\t" '{if($(NF-3)=="Yes"){count=count+1}}END{print count}'`
countLTE=`cat mapped_all.txt | awk -F"\t" '{if($(NF-4)=="Yes"){count=count+1}}END{print count}'`
countHSCSD=`cat mapped_all.txt | awk -F"\t" '{if($(NF-5)=="Yes"){count=count+1}}END{print count}'`
countHSDPA=`cat mapped_all.txt | awk -F"\t" '{if($(NF-6)=="Yes"){count=count+1}}END{print count}'`
countEDGE=`cat mapped_all.txt | awk -F"\t" '{if($(NF-7)=="Yes"){count=count+1}}END{print count}'`
countGPRS=`cat mapped_all.txt | awk -F"\t" '{if($(NF-8)=="Yes"){count=count+1}}END{print count}'`
countGSM=`cat mapped_all.txt | awk -F"\t" '{if($(NF-9)=="Yes"){count=count+1}}END{print count}'`
totalcount=`wc -l mapped_all.txt | awk '{print $1}'`

requestsSyncML=`cat mapped_all.txt | awk -F"\t" '{if($(NF-1)=="Yes"){count=count+$2}}END{print count}'`
requestsXHTML=`cat mapped_all.txt | awk -F"\t" '{if($(NF-2)=="Yes"){count=count+$2}}END{print count}'`
requestsWAP=`cat mapped_all.txt | awk -F"\t" '{if($(NF-3)=="Yes"){count=count+$2}}END{print count}'`
requestsLTE=`cat mapped_all.txt | awk -F"\t" '{if($(NF-4)=="Yes"){count=count+$2}}END{print count}'`
requestsHSCSD=`cat mapped_all.txt | awk -F"\t" '{if($(NF-5)=="Yes"){count=count+$2}}END{print count}'`
requestsHSDPA=`cat mapped_all.txt | awk -F"\t" '{if($(NF-6)=="Yes"){count=count+$2}}END{print count}'`
requestsEDGE=`cat mapped_all.txt | awk -F"\t" '{if($(NF-7)=="Yes"){count=count+$2}}END{print count}'`
requestsGPRS=`cat mapped_all.txt | awk -F"\t" '{if($(NF-8)=="Yes"){count=count+$2}}END{print count}'`
requestsGSM=`cat mapped_all.txt | awk -F"\t" '{if($(NF-9)=="Yes"){count=count+$2}}END{print count}'`
totalrequests=`cat mapped_all.txt | awk -F"\t" '{count=count+$(NF-10)}END{print count}'`

percentSyncML=`echo ${countSyncML} ${totalcount} | awk '{print $1*100/$2}'`
percentXHTML=`echo ${countXHTML} ${totalcount} | awk '{print $1*100/$2}'`
percentWAP=`echo ${countWAP} ${totalcount} | awk '{print $1*100/$2}'`
percentLTE=`echo ${countLTE} ${totalcount} | awk '{print $1*100/$2}'`
percentHSCSD=`echo ${countHSCSD} ${totalcount} | awk '{print $1*100/$2}'`
percentHSDPA=`echo ${countHSDPA} ${totalcount} | awk '{print $1*100/$2}'`
percentEDGE=`echo ${countEDGE} ${totalcount} | awk '{print $1*100/$2}'`
percentGPRS=`echo ${countGPRS} ${totalcount} | awk '{print $1*100/$2}'`
percentGSM=`echo ${countGSM} ${totalcount} | awk '{print $1*100/$2}'`

percentRequestsSyncML=`echo ${requestsSyncML} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsXHTML=`echo ${requestsXHTML} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsWAP=`echo ${requestsWAP} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsLTE=`echo ${requestsLTE} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsHSCSD=`echo ${requestsHSCSD} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsHSDPA=`echo ${requestsHSDPA} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsEDGE=`echo ${requestsEDGE} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsGPRS=`echo ${requestsGPRS} ${totalrequests} | awk '{print $1*100/$2}'`
percentRequestsGSM=`echo ${requestsGSM} ${totalrequests} | awk '{print $1*100/$2}'`

echo SyncML ${percentSyncML} ${percentRequestsSyncML}
echo XHTML ${percentXHTML} ${percentRequestsXHTML}
echo WAP ${percentWAP} ${percentRequestsWAP}
echo LTE ${percentLTE} ${percentRequestsLTE}
echo HSCSD ${percentHSCSD} ${percentRequestsHSCSD}
echo HSDPA ${percentHSDPA} ${percentRequestsHSDPA}
echo EDGE ${percentEDGE} ${percentRequestsEDGE}
echo GPRS ${percentGPRS} ${percentRequestsGPRS}
echo GSM ${percentGSM} ${percentRequestsGSM}

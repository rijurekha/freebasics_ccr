from fuzzywuzzy import fuzz
from fuzzywuzzy import process

phones = open('phones_pak.txt', 'r')
finalfile = open('mapped_pak.txt', 'w')

# Given the brand of phone has been identified, write phone to file
def writePhoneToFile(phone, phonelist, namelist, file):
	# Search for exact match
	if (phone[0] in namelist):
			idx = namelist.index(phone[0])
			toInsert = phonelist[idx]
			print "exact match\t",str(phone[0]),"\t",str(toInsert)
			first = True
			for field in toInsert:
				if first:
					first = False
					continue
				else:
					phone.append(field)
	# If no exact match, find the one with highest partial match
	else:
		maxscore = 0
		idx = 0
		for currPhone in namelist:
			# partial_ratio is the edit distance between the two strings
			score = fuzz.partial_ratio(currPhone, phone[0])
			if (score > maxscore):
				maxscore = score
				idx = namelist.index(currPhone)
		toInsert = phonelist[idx]
		print str(maxscore)," match\t",str(phone[0]),"\t",str(toInsert)
		first = True
		for field in toInsert:
			if first:
				first = False
				continue
			else:
				phone.append(field)
	for field in phone:
		file.write(field.rstrip('\n') + '\t')
	file.write('\n')

acer = open('acer.txt', 'r')
alcatel = open('alcatel.txt', 'r')
apple = open('apple.txt', 'r')
asus = open('asus.txt', 'r')
blu = open('blu.txt', 'r')
cherry = open('cherry.txt', 'r')
evercoss = open('evercoss.txt', 'r')
gionee = open('gionee.txt', 'r')
htc = open('htc.txt', 'r')
huawei = open('huawei.txt', 'r')
#i-mobile = open('i-mobile.txt', 'r')
infinix = open('infinix.txt', 'r')
lanix = open('lanix.txt', 'r')
lava = open('lava.txt', 'r')
lenovo = open('lenovo.txt', 'r')
lg = open('lg.txt', 'r')
micromax = open('micromax.txt', 'r')
motorola = open('motorola.txt', 'r')
myphones = open('myphones.txt', 'r')
nokia = open('nokia.txt', 'r')
#o+ = open('o+.txt', 'r')
oppo = open('oppo.txt', 'r')
qmobile = open('qmobile.txt', 'r')
samsung = open('samsung.txt', 'r')
sony = open('sony.txt', 'r')
sonyericsson = open('sonyericsson.txt', 'r')
starmobile = open('starmobile.txt', 'r')
symphony = open('symphony.txt', 'r')
tecno = open('tecno.txt', 'r')
vivo = open('vivo.txt', 'r')
vodafone = open('vodafone.txt', 'r')
walton = open('walton.txt', 'r')
xiaomi = open('xiaomi.txt', 'r')
zte = open('zte.txt', 'r')

weird = open('weird.txt', 'w')

phonelist = []

# REPETITION! NEED TO MAKE A LIST OF ALL THESE LISTS
acerlist = []
alcatellist = []
applelist = []
asuslist = []
blulist = []
cherrylist = []
evercosslist = []
gioneelist = []
htclist = []
huaweilist = []
#i-mobilelist = []
infinixlist = []
lanixlist = []
lavalist = []
lenovolist = []
lglist = []
micromaxlist = []
motorolalist = []
myphoneslist = []
nokialist = []
#o+list = []
oppolist = []
phones_tomaplist = []
qmobilelist = []
samsunglist = []
sonylist = []
sonyericssonlist = []
starmobilelist = []
symphonylist = []
tecnolist = []
vivolist = []
vodafonelist = []
waltonlist = []
xiaomilist = []
ztelist = []

acerphones = []
alcatelphones = []
applephones = []
asusphones = []
bluphones = []
cherryphones = []
evercossphones = []
gioneephones = []
htcphones = []
huaweiphones = []
#i-mobilephones = []
infinixphones = []
lanixphones = []
lavaphones = []
lenovophones = []
lgphones = []
micromaxphones = []
motorolaphones = []
myphonesphones = []
nokiaphones = []
#o+phones = []
oppophones = []
phones_tomapphones = []
qmobilephones = []
samsungphones = []
sonyphones = []
sonyericssonphones = []
starmobilephones = []
symphonyphones = []
tecnophones = []
vivophones = []
vodafonephones = []
waltonphones = []
xiaomiphones = []
ztephones = []

# REPETITION! NEED TO MAKE A FUNCTION FOR THIS
for line in phones:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	phonelist.append(separatedList)

for line in acer:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	acerphones.append(separatedList[0])
	acerlist.append(separatedList)

for line in alcatel:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	alcatelphones.append(separatedList[0])
	alcatellist.append(separatedList)

for line in apple:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	applephones.append(separatedList[0])
	applelist.append(separatedList)

for line in asus:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	asusphones.append(separatedList[0])
	asuslist.append(separatedList)

for line in blu:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	bluphones.append(separatedList[0])
	blulist.append(separatedList)

for line in cherry:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	cherryphones.append(separatedList[0])
	cherrylist.append(separatedList)

for line in evercoss:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	evercossphones.append(separatedList[0])
	evercosslist.append(separatedList)

for line in gionee:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	gioneephones.append(separatedList[0])
	gioneelist.append(separatedList)

for line in htc:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	htcphones.append(separatedList[0])
	htclist.append(separatedList)

for line in huawei:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	huaweiphones.append(separatedList[0])
	huaweilist.append(separatedList)

#for line in i-mobile:
#	separatedList = [splits for splits in line.split("\t") if splits is not ""]
#	separatedList[0] = separatedList[0].lower()
#	i-mobilephones.append(separatedList[0])
#	i-mobilelist.append(separatedList)

for line in infinix:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	infinixphones.append(separatedList[0])
	infinixlist.append(separatedList)

for line in lanix:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	lanixphones.append(separatedList[0])
	lanixlist.append(separatedList)

for line in lava:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	lavaphones.append(separatedList[0])
	lavalist.append(separatedList)

for line in lenovo:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	lenovophones.append(separatedList[0])
	lenovolist.append(separatedList)

for line in lg:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	lgphones.append(separatedList[0])
	lglist.append(separatedList)

for line in micromax:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	micromaxphones.append(separatedList[0])
	micromaxlist.append(separatedList)

for line in motorola:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	motorolaphones.append(separatedList[0])
	motorolalist.append(separatedList)

for line in myphones:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	myphonesphones.append(separatedList[0])
	myphoneslist.append(separatedList)

for line in nokia:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	nokiaphones.append(separatedList[0])
	nokialist.append(separatedList)

#for line in o+:
#	separatedList = [splits for splits in line.split("\t") if splits is not ""]
#	separatedList[0] = separatedList[0].lower()
#	o+phones.append(separatedList[0])
#	o+list.append(separatedList)

for line in oppo:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	oppophones.append(separatedList[0])
	oppolist.append(separatedList)

for line in qmobile:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	qmobilephones.append(separatedList[0])
	qmobilelist.append(separatedList)

for line in samsung:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	samsungphones.append(separatedList[0])
	samsunglist.append(separatedList)

for line in sony:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	sonyphones.append(separatedList[0])
	sonylist.append(separatedList)

for line in sonyericsson:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	sonyericssonphones.append(separatedList[0])
	sonyericssonlist.append(separatedList)

for line in starmobile:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	starmobilephones.append(separatedList[0])
	starmobilelist.append(separatedList)

for line in symphony:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	symphonyphones.append(separatedList[0])
	symphonylist.append(separatedList)

for line in tecno:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	tecnophones.append(separatedList[0])
	tecnolist.append(separatedList)

for line in vivo:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	vivophones.append(separatedList[0])
	vivolist.append(separatedList)

for line in vodafone:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	vodafonephones.append(separatedList[0])
	vodafonelist.append(separatedList)

for line in walton:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	waltonphones.append(separatedList[0])
	waltonlist.append(separatedList)

for line in xiaomi:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	xiaomiphones.append(separatedList[0])
	xiaomilist.append(separatedList)

for line in zte:
	separatedList = [splits for splits in line.split("\t") if splits is not ""]
	separatedList[0] = separatedList[0].lower()
	ztephones.append(separatedList[0])
	ztelist.append(separatedList)

# Go over the entire list of phones
counter = 0
for phone in phonelist:
	if ('acer' in phone[0]):
		writePhoneToFile(phone, acerlist, acerphones, finalfile)

	elif ('alcatel' in phone[0]):
		writePhoneToFile(phone, alcatellist, alcatelphones, finalfile)

	elif ('apple' in phone[0]):
		writePhoneToFile(phone, applelist, applephones, finalfile)

	elif ('asus' in phone[0]):
		writePhoneToFile(phone, asuslist, asusphones, finalfile)

	elif ('blu' in phone[0]):
		writePhoneToFile(phone, blulist, bluphones, finalfile)

	elif ('cherry' in phone[0]):
		writePhoneToFile(phone, cherrylist, cherryphones, finalfile)

	elif ('evercoss' in phone[0]):
		writePhoneToFile(phone, evercosslist, evercossphones, finalfile)

	elif ('gionee' in phone[0]):
		writePhoneToFile(phone, gioneelist, gioneephones, finalfile)

	elif ('htc' in phone[0]):
		writePhoneToFile(phone, htclist, htcphones, finalfile)

	elif ('huawei' in phone[0]):
		writePhoneToFile(phone, huaweilist, huaweiphones, finalfile)

#	elif ('i-mobile' in phone[0]):
#		writePhoneToFile(phone, i-mobilelist, i-mobilephones, finalfile)

	elif ('infinix' in phone[0]):
		writePhoneToFile(phone, infinixlist, infinixphones, finalfile)

	elif ('lanix' in phone[0]):
		writePhoneToFile(phone, lanixlist, lanixphones, finalfile)

	elif ('lava' in phone[0]):
		writePhoneToFile(phone, lavalist, lavaphones, finalfile)

	elif ('lenovo' in phone[0]):
		writePhoneToFile(phone, lenovolist, lenovophones, finalfile)

	elif ('lg' in phone[0]):
		writePhoneToFile(phone, lglist, lgphones, finalfile)

	elif ('micromax' in phone[0]):
		writePhoneToFile(phone, micromaxlist, micromaxphones, finalfile)

	elif ('motorola' in phone[0]):
		writePhoneToFile(phone, motorolalist, motorolaphones, finalfile)

	elif ('myphones' in phone[0]):
		writePhoneToFile(phone, myphoneslist, myphonesphones, finalfile)

	elif ('nokia' in phone[0]):
		writePhoneToFile(phone, nokialist, nokiaphones, finalfile)

#	elif ('o+' in phone[0]):
#		writePhoneToFile(phone, o+list, o+phones, finalfile)

	elif ('oppo' in phone[0]):
		writePhoneToFile(phone, oppolist, oppophones, finalfile)

	elif ('qmobile' in phone[0]):
		writePhoneToFile(phone, qmobilelist, qmobilephones, finalfile)

	elif ('samsung' in phone[0]):
		writePhoneToFile(phone, samsunglist, samsungphones, finalfile)

	elif ('sony' in phone[0]):
		writePhoneToFile(phone, sonylist, sonyphones, finalfile)

	elif ('sonyericsson' in phone[0]):
		writePhoneToFile(phone, sonyericssonlist, sonyericssonphones, finalfile)

	elif ('starmobile' in phone[0]):
		writePhoneToFile(phone, starmobilelist, starmobilephones, finalfile)

	elif ('symphony' in phone[0]):
		writePhoneToFile(phone, symphonylist, symphonyphones, finalfile)

	elif ('tecno' in phone[0]):
		writePhoneToFile(phone, tecnolist, tecnophones, finalfile)

	elif ('vivo' in phone[0]):
		writePhoneToFile(phone, vivolist, vivophones, finalfile)

	elif ('vodafone' in phone[0]):
		writePhoneToFile(phone, vodafonelist, vodafonephones, finalfile)

	elif ('walton' in phone[0]):
		writePhoneToFile(phone, waltonlist, waltonphones, finalfile)

	elif ('xiaomi' in phone[0]):
		writePhoneToFile(phone, xiaomilist, xiaomiphones, finalfile)

	elif ('zte' in phone[0]):
		writePhoneToFile(phone, ztelist, ztephones, finalfile)

	else:
		counter = counter + 1
		weird.write(phone[0])
		weird.write('\n')

print "Total phones not found: " + str(counter)

from __future__ import unicode_literals
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os, sys, subprocess
import codecs
from collections import Counter, defaultdict
import pickle
from datetime import datetime as dt

##############

## Set if behind a HTTP proxy
# proxy = "http://10.3.100.207:8080"

timeFmt                         =   "%Y-%m-%d %H:%M:%S   "

pathFreqMobileDevicesPickle     =   "pickled/freqMobileDevices.pkl"
pathFreqUserAgentsStatePickle   =   "pickled/freqUserAgentsState.pkl"
pathCounterPickle               =   "pickled/counter.pkl"

pathUserAgents                  =   "user-agents.txt"
#pathUserAgents                 =   "sampled-user-agents.txt"
pathUniqueUserAgents            =   "unique-user-agents.txt"
pathMobileDevices               =   "mobile-devices.txt"
pathUserAgentsState             =   "user-agents-identified.txt"
pathUserAgentsMissed            =   "user-agents-missed.txt"
pathUserAgentMappings           =   "user-agent-mapping.txt"

##############

def cprint(message):
    """
        Print customized message with timestamp
    """

    print "{} {}".format(dt.strftime(dt.now(), timeFmt), message)

##############

def auto_detect():
    """
        Detect mobile device for all user-agents
    """

    fp              =   codecs.open(pathUserAgents, "rb", encoding="utf-8")
    allUserAgents   =   [l.strip() for l in fp.readlines()]
    fp.close()
    cprint("Extracted user agents")

    freqUserAgents  =   Counter(allUserAgents)
    userAgentsAll   =   freqUserAgents.keys()

    fp              =   codecs.open(pathUniqueUserAgents, "wb", encoding="utf-8")
    [fp.write("{}___{}\n".format(ua, str(freqUserAgents[ua]))) for ua in sorted(set(allUserAgents))]
    fp.close()
    cprint("Saved unique user agent counts to file")

    freqMobileDevices   =   defaultdict(int)
    freqUserAgentsState =   set()
    userAgentMappings   =   []
    counter             =   0
    length              =   len(freqUserAgents)

    if os.path.exists(pathFreqMobileDevicesPickle):
        freqMobileDevices   =   pickle.load(open(pathFreqMobileDevicesPickle, "rb"))
        [freqUserAgentsState.add(md) for md in freqMobileDevices.keys()]
        counter             =   pickle.load(open(pathCounterPickle, "rb"))
        cprint("Pickle exists, loaded data for {} devices".format(counter))
    cprint("Created hashmap of user agents")

    while counter<=length:
   
        currentUserAgent = ""

        try:
            #chromedriver   =   "/home/satadal/.bundle/utilities/chromedriver"
            phantomjs       =   "/usr/local/bin/phantomjs"
            #os.environ["webdriver.chrome.driver"] = chromedriver
            #webDriver      =   webdriver.Chrome(chromedriver)
            webDriver       =   webdriver.PhantomJS(executable_path=phantomjs, service_args=['--ignore-ssl-errors=true', '--ssl-protocol=any'])
            cprint("Obtained web driver")

            siteAddress     =   "http://tools.scientiamobile.com/"
            webDriver.get(siteAddress)
            cprint("Loaded webpage")

            while userAgentsAll:
                ua = userAgentsAll[0]
                if ua not in freqUserAgentsState:
                    currentUserAgent    =   ua
                    mobile_device       =   auto_detect_one(webDriver, ua)
                    frequency           =   freqUserAgents[ua]
                    freqMobileDevices[mobile_device] += frequency
                    freqUserAgentsState.add(ua)
                    userAgentMappings.append((ua,mobile_device))
                    counter             += 1
                    if counter%25 == 0:
                        cprint("Completed {} of {}".format(counter, length))
                        saveToFile(freqUserAgents, freqMobileDevices, freqUserAgentsState, userAgentMappings, counter)
                        #pickle.dump(freqMobileDevices, open(pathFreqMobileDevicesPickle, "wb"))
                userAgentsAll.remove(ua)

        except Exception, e:
            cprint("Crashed for user-agent: {}".format(currentUserAgent))
            cprint("Crash message:")
            print(str(e))
            #webDriver.save_screenshot('screen.png')
            #fpe = open("source.html","w")
            #fpe.write(webDriver.page_source)
            #fpe.close()
                
        finally:
            try:
                webDriver.close()
                cprint("Webdriver closed")
            except:
                None
            finally:
                cprint("Relaunch")
        
        if counter >= length:
            break
    
    saveToFile(freqUserAgents, freqMobileDevices, freqUserAgentsState, userAgentMappings, counter)

#############

def saveToFile(freqUserAgents, freqMobileDevices, freqUserAgentsState, userAgentMappings, counter):

    flag = True
    
    try:
        fp = codecs.open(pathMobileDevices, "wb", encoding="utf-8")
        [fp.write("{}___{}\n".format(i,str(freqMobileDevices[i]))) for i in sorted(freqMobileDevices.keys())]
        fp.close()
    
        fp = codecs.open(pathUserAgentsState, "wb", encoding="utf-8")
        [fp.write("{}\n".format(ua)) for ua in sorted(freqUserAgentsState)]
        fp.close()

        fp = codecs.open(pathUserAgentsMissed, "wb", encoding="utf-8")
        freqUserAgentsMissed = [ua for ua in freqUserAgents.keys() if ua not in freqUserAgentsState]
        [fp.write("{}\n".format(ua)) for ua in sorted(freqUserAgentsMissed)]
        fp.close()

        fp = codecs.open(pathUserAgentMappings, "wb", encoding="utf-8")
        [fp.write("{}___{}\n".format(ua[0], ua[1])) for ua in sorted(userAgentMappings, key=lambda x: x[0])]
        fp.close()

        #cprint("Data saved in file and pickled.")
    except Exception, e:
        cprint("Crashed while saving to file.")
        cprint("Crash message:")
        print str(e)
        flag = False

    finally:
        
        try:
            pickle.dump(freqMobileDevices, open(pathFreqMobileDevicesPickle, "wb"))
            pickle.dump(freqUserAgentsState, open(pathFreqUserAgentsStatePickle, "wb"))
            pickle.dump(counter, open(pathCounterPickle, "wb"))
            #cprint("Pickled instead.")
            if flag:
                cprint("Saved and pickled.")
            else:
                cprint("Pickled instead.")
        
        except Exception, ep:
            cprint("Crashed while pickling.")
            cprint("Crash message:")
            print str(ep)
            cprint("FATAL! Will exit.")
            sys.exit()

#############

def auto_detect_one(webDriver, userAgent):
    """
        Connects to tools.scientiamobile.com
        and fetches user-agent
    """

    elementTextArea     =   webDriver.find_element_by_name("user-agent-string")
    elementTextArea.clear()
    elementTextArea.send_keys(userAgent)
    elementTextArea.submit()

    elementResponseText =   webDriver.find_elements_by_class_name("monospace")[-1]
    mobileDevice        =   elementResponseText.get_attribute("innerHTML")
    
    return mobileDevice

###############

def main():
    """
        Main method
    """
    
    if not os.path.exists("pickled"):
        os.mkdir("pickled")


    ## Required only if you are behind a HTTP proxy
    """
    if len(proxy)>1:
        subprocess.call("export http_proxy={}".format(proxy), shell=True)
        subprocess.call("export https_proxy={}".format(proxy), shell=True)
    else:
        subprocess.call("unset http_proxy", shell=True)
        subprocess.call("unset https_proxy", shell=True)
    """

    auto_detect()

###############

if __name__ == "__main__":
    main()

# README #

### What is this repository for? ###
The useragents generated in 1_pcaps_to_useragents/ are mapped to mobile devices using http://tools.scientiamobile.com/.

### Python dependency ###
Selenium

### File description ###
1. There are four sub-folders: learnbasics-all, learnbasics-pak, newsbugle-all, newsbugle-pak, corresponding to useragent files generated in [1_pcaps_to_useragents/](../1_pcaps_to_useragents): learnbasics_useragents_all.txt, learnbasics_useragents_pak.txt, newsbugle_useragents_all.txt, newsbugle_useragents_pak.txt respectively. The useragent files are copied from   1_pcaps_to_useragents/ and renamed to user-agents.txt in each sub-folder.

2. The auto_device_detect.py needs to be copied to and run in each sub-folder. It processes the user-agents.txt files and maps the user
agents to mobile devices using the Scientia Mobile website http://tools.scientiamobile.com/. Multiple intermediate files are created in each sub-folder, with the main output in mobile-devices.txt.

3. mobile-devices.txt is generated in each sub-folder, where each line contains the name of the mobile device, the link from which this device information is obtained and ___count.

4. getmobile.sh needs to be copied into each sub-folder and run to generate devices_count.txt from mobile-devices.txt, removing the link and summing the counts for each device name.  

### How to run ###
$python auto_device_detect.py

$./getmobile.sh | tee devices_count.txt

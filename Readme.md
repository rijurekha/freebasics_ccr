# README #

### What is this repository for? ###

To make our results reproducible, we have created this public repository of the code and data used in the project. As described in the paper, the experimental setups for this work
have been non-trivial. On the client side, we needed a laptop, tethered on Wi-Fi to a mobile phone, that acted as a Wi-Fi hotspot. The setup is shown the figure below. This mobile had a SIM card with Free Basics support for that country and also had credit balance to conduct the non Free Basics normal cellular connection experiments. We had these client setups in LUMS, Pakistan and UCT, South Africa. In Pakistan, we conducted experiments using SIM cards from two cellular service providers:
Zong and Telenor. In South Africa, we conducted experiments with the single cellular provider supporting Free Basics, namely Cell C.

![Alt text](setup.jpg?raw=true "Client side setup")

In addition to the client side measurement setup, we implemented and controlled two web servers for Bugle News and Learn Basics, hosted at MPI-SWS in Germany. We registered these services to be part of Free Basics service list,
going through Facebook's online application and approval process. We also moved the physical hosting of the Learn Basics server across Virginia, Sao Paolo, Mumbai, Sydney
and Tokyo, using Amazon EC2 instances, for deconstructing the proxy architecture and network path inflation details.

If someone wants to conduct the experiments again, please get in touch with us at rijurekha@mpi-sws.org. Both our web services are live on Free Basics, continuously accumulating visits from the ever growing number of Free Basics countries. Our collaborators in Pakistan and South Africa, who helped us conduct the client side measurements, are our co-authors in this paper. Thus they can help to conduct similar client side measurements in future, in these two countries. In this public repository, we make available all the data collected in our experiments. We also include the code used
to run the experiments, and the analysis code used in processing the data and generating the results and the graphs.

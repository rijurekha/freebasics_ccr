#Riju
# This is the crawler to fetch a large file over paid connection for throughput experiments.
# It runs on a laptop, tethered over Wi-Fi to a mobile phone with Free Basics SIM Card, acting as Wi-Fi hotspot.
# It opens a mechanized browser, accesses our Learn Basics url and reaches the landing page.
# It looks for image.html (the large file specially created for this experiment)
# It then fetches this file 10 times in a loop.
# tcpdump captures the requests in a pcap file, running on the same laptop.
# mitmproxy captures the raw requests/responses.

from bs4 import BeautifulSoup
import urllib2
import urllib
import time
import os
import random
import ssl
from cookielib import CookieJar
import mechanize
import re

cj = CookieJar()

def main():
	if hasattr(ssl, '_create_unverified_context'):
		ssl._create_default_https_context = ssl._create_unverified_context

	try:
		os.remove("log.txt")
	except OSError:
		pass

	expmnt_time = time.strftime("%c")
	expmnt_time = expmnt_time.replace (" ", "_")
	write_log(expmnt_time)
	loc = "HTMLs_" + str(expmnt_time)

	try:
		os.mkdir(loc)
	except:
		pass

	save_service("http://learnbasics.mpi-sws.org",loc+"/learnbasics") 

def save_service(url,dest):
	global cj	
	br = mechanize.Browser()
	br.set_handle_equiv(True)
	br.set_handle_refresh(False)
	br.set_handle_redirect(True)
	br.set_handle_referer(True)
	br.set_handle_robots(False)
	br.set_cookiejar(cj)
	br.addheaders = [('User-agent', "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19 Amreesh")]
	br.set_request_gzip(True)
	write_log(url)
	try:
		os.mkdir(dest)
	except Exception:
		pass
	br.clear_history()
	res = br.open(url)
	service1 = res.read()
	page_name = os.path.join(dest, "service1.html")
	with open(page_name, "wb") as fid:
		fid.write(service1)
        for link in br.links():
		if("image" in link.url):
			break
	for i in range(0, 10):	
		br.clear_history()
		request = br.click_link(link)
		res = br.open(request)
		service2 = res.read()
		page_name = os.path.join(dest, "service2"+str(i)+".html")
		with open(page_name, "wb") as fid:
			fid.write(service2)
	
def write_log(s):
	with open("log.txt","a") as fid:
		fid.write(s)
		fid.write("\n")

def finish():
	print "Later..."

if __name__ == "__main__":
	main()

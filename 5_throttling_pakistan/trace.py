import subprocess
import sys
import ipaddress
import numpy
import copy
import os
class Connection(object):
    '''
    TCPTRACE gives separate stats for each connection (a.k.a stream) in the trace. 
    
    This class stores tcptrace stats for a single stream
    '''
    def __init__(self, statsText=None):
        self.connNum     = None
        self.connStats   = {}
        self.detailStats = {'client2server' : {},
                            'server2client' : {}}
        if statsText:
            self.parseTcptraceOutput(statsText)
    
    def parseTcptraceOutput(self, statsText):
        self.connNum = int(statsText[0].rpartition(':')[0].split(' ')[2])
        
        [aIP, aPort] = statsText[1].partition(':')[2].strip().split(':')
        [bIP, bPort] = statsText[2].partition(':')[2].strip().split(':')
        
        host1 = statsText[1].partition(':')[0].split(' ')[1]
        host2 = statsText[2].partition(':')[0].split(' ')[1]
       
	aIPstring = ""+aIP
	if aIPstring.startswith("139."): 
            self.serverName, self.serverIP, self.serverPort = host1, aIP, aPort
            self.clientName, self.clientIP, self.clientPort = host2, bIP, bPort
        else:
            self.clientName, self.clientIP, self.clientPort = host1, aIP, aPort
            self.serverName, self.serverIP, self.serverPort = host2, bIP, bPort
	
	self.lastPacket = statsText[5].partition(':')[2]
	
	#print self.clientIP, self.clientPort
        
        columns = map(lambda x: x.strip(), statsText[9][:-1].split(':'))

        if columns[0] == '{}->{}'.format(self.clientName, self.serverName):
            self.client2serverColumn = 0
            self.server2clientColumn = 1
        else:
            self.client2serverColumn = 1
            self.server2clientColumn = 0
        
        for l in statsText[3:10]:
            self.connStats[l.split(':')[0].strip()] = l.split(':')[1].strip()
            
        for l in statsText[10:]:
            key    = l.partition(':')[0]
            values = [l.partition(':')[2].partition(key+':')[0].strip(), l.partition(':')[2].partition(key+':')[2].strip()]
            
            self.detailStats['client2server'][key] = values[self.client2serverColumn]
            self.detailStats['server2client'][key] = values[self.server2clientColumn]
            
class TcptraceResult(object):
    '''
    A class which stores "tcptrace" stats for a whole pcap file
    '''
    def __init__(self, pcapPath):
        self.pcapPath            = pcapPath
        self.connections         = []
        self.connectionsFiltered = []
        self.includedInFiltered  = []
        self.totalXputMin_tshark = None
        self.totalXputMax_tshark = None
        self.totalXputAvg_tshark = None
        self.totalRetransmits    = None
        self.totalOutOfOrder     = None
        self.clientMinWinAdv     = None
        self.clientMaxWinAdv     = None
        self.serverMinWinAdv     = None
        self.serverMaxWinAdv     = None
        self.numberOfTcpResets   = None

    def doTotals(self):
        '''
        Produce some stats for the whole pcap file considering all the connections
        '''
        self.totalDataPackets       = sum([int(x.detailStats['server2client']['actual data pkts']) for x in self.connections]
                                          +
                                          [int(x.detailStats['client2server']['actual data pkts']) for x in self.connections])
        
        self.totalRetransmits       = sum([int(x.detailStats['server2client']['rexmt data pkts']) for x in self.connections]
                                          +
                                          [int(x.detailStats['client2server']['rexmt data pkts']) for x in self.connections])
    
        self.totalOutOfOrder        = sum([int(x.detailStats['server2client']['outoforder pkts']) for x in self.connections]
                                          +
                                          [int(x.detailStats['client2server']['outoforder pkts']) for x in self.connections])
        
        self.clientMinWinAdv        = min([int(x.detailStats['client2server']['min win adv'].split()[0]) for x in self.connections])
        self.clientMaxWinAdv        = max([int(x.detailStats['client2server']['max win adv'].split()[0]) for x in self.connections])
        self.serverMinWinAdv        = min([int(x.detailStats['server2client']['min win adv'].split()[0]) for x in self.connections])
        self.serverMaxWinAdv        = max([int(x.detailStats['server2client']['max win adv'].split()[0]) for x in self.connections])
        
        self.server2clientMinRTT    = min([float(x.detailStats['server2client']['RTT min'].split()[0]) for x in self.connections])
        self.server2clientMaxRTT    = max([float(x.detailStats['server2client']['RTT max'].split()[0]) for x in self.connections])
        
        self.server2clientAvgRTTavg = round(numpy.average([float(x.detailStats['server2client']['RTT avg'].split()[0]) for x in self.connections]), 2)
        self.server2clientAvgRTTstd = round(numpy.average([float(x.detailStats['server2client']['RTT stdev'].split()[0]) for x in self.connections]), 2)
        
def doOne(pcapFile):
    #1-Create tcptrace result object
    tcptraceRes = TcptraceResult(pcapFile)
    
    #3-Run tcptrace and get the output
    cmd         = ['tcptrace', '-l', '-n', '-r', '-W', pcapFile]
    p           = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
    output, err = p.communicate()
    output      = output.split('\n')
   
    #4-Find the begining of stats
    for i in range(len(output)):
        l = output[i].strip()
        
        if l.endswith('TCP connections traced:'):
            index = i
            break
    
    #5-Parse results one connection at a time and add to the result object
    statsText = []
    for i in range(index+1, len(output)):
        l = output[i].strip()
        
        if not l.startswith('====='):
            statsText.append(l)
        else:
            conn = Connection(statsText)
            tcptraceRes.connections.append(conn)
            statsText = []
    
    tcptraceRes.doTotals()
    return tcptraceRes

def main():
	folder = '/path/to/pcap/files/'
	count=0
	all = {}
	pcap_files=[]
	pcap_files += [each for each in os.listdir(folder) if each.endswith('.pcap')]
	for pcapFile in pcap_files:
		filename = ""+pcapFile
		outfile = filename.replace(".pcap","_throughput.txt")
		tcptraceRes = doOne(pcapFile)
        	all[ pcapFile.rpartition('/')[2] ] = tcptraceRes

		f = open(outfile, 'w')       
        	for conn in (tcptraceRes.connections):
	     		print ','.join(map(str, [conn.connNum, conn.detailStats['server2client']['throughput'], conn.detailStats['client2server']['throughput']]))
	     		f.write(','.join(map(str, [conn.connNum,  conn.detailStats['server2client']['throughput'], conn.detailStats['client2server']['throughput'], conn.detailStats['server2client']['total packets'], conn.detailStats['server2client']['unique bytes sent'], conn.detailStats['server2client']['actual data pkts'], conn.detailStats['server2client']['actual data bytes'], conn.detailStats['client2server']['total packets'], conn.detailStats['client2server']['unique bytes sent'], conn.detailStats['client2server']['actual data pkts'], conn.detailStats['client2server']['actual data bytes']]))+"\n")
       
        	f.close()
    
if __name__ == '__main__':
    main()    

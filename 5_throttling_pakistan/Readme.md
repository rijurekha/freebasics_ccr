# README #

### What is this repository for? ###
This contains the crawlers frb_crawl.py and nfrb_crawl.py to fetch a large file image.html from Learn Basics server, over Free Basics and paid cellular connection respectively.

### Python dependency ###
mechanize
 
### File description ###
* frb_crawl.py opens a mechanized browser, accesses the Free Basics URL, goes through all the service lists. It clicks on the Learn Basics service (ID 253471785031826) and adds it. It then opens the learnbasics home page, and fetches image.html 10 times. This crawler runs on a laptop, tethered over Wi-Fi to a mobile phone with Free Basics SIM Card, acting as Wi-Fi hotspot. The landing pages are different when the URL http://www.freebasics.com is accessed directly from a laptop, vs. through Free Basics. The script will crash when called without the Free Basics SIM. The landing page in that case won't match the script's expectation.

* nfrb_crawl.py opens a mechanized browser, opens the learnbasics home page, and fetches image.html 10 times.

* trace.py computes the throughput from the clientside and serverside tcpdump files in pcap format. frb1.pcap and nfrb1.pcap are included as examples of client-side tcpdump files. When trace.py is run on nfrb1.pcap, the last 2 entries (for the last 2 fetches of image.html) are
```
11,621666 Bps,77 Bps

12,659883 Bps,82 Bps
```
The second column gives the paid cellular connection throughput. When trace.py is run on frb1.pcap, the last 2 entries (for the last 2 fetches of image.html) are
```
32,410623 Bps,151 Bps

33,399912 Bps,147 Bps
```
The second column gives the Free Basics throughput.

* The sub-folders contain the cdf of throughput of Free Basics access (frb) and direct cellular access (nfrb), for both server and client-side tcpdumps. The sub-folders are named [telenor](telenor) and [zong](zong) for the two telecom providers we experimented with in Pakistan. plot.gnu plots the cdf files into eps graphs.
### How to run ###
$python frb_crawl.py

$python nfrb_crawl.py

$python trace.py

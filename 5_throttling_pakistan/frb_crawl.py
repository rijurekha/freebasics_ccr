#Riju: 
# This is the crawler to fetch a large file over Free Basics for throughput experiments.
# It runs on a laptop, tethered over Wi-Fi to a mobile phone with Free Basics SIM Card, acting as Wi-Fi hotspot.
# It opens a mechanized browser, accesses the Free Basics URL, goes through all the service lists.
# It clicks on the Learn Basics service (ID 253471785031826) and adds it.
# In case of 500 server error, it emulates a "back" button press and a "submit" click, to add the service.
# It then opens the learnbasics home page, and looks for image.html (the large file specially created for this experiment)
# It then fetches this file 10 times in a loop.
# tcpdump captures the requests in a pcap file, running on the same laptop.
# mitmproxy captures the raw requests/responses (the filenames are not available in tcpdump, as Facebook uses https between the mobile
# and the first proxy, thereby encrypting data).

from bs4 import BeautifulSoup
import urllib2
import urllib
import time
import os
import random
import ssl
from cookielib import CookieJar
import mechanize
import requests
import re

cj = CookieJar()

def main():
	if hasattr(ssl, '_create_unverified_context'):
		ssl._create_default_https_context = ssl._create_unverified_context

	try:
		os.remove("log.txt")
	except OSError:
		pass

	expmnt_time = time.strftime("%c")
	expmnt_time = expmnt_time.replace (" ", "_")
	write_log(expmnt_time)
	loc = "HTMLs_" + str(expmnt_time)

	try:
		os.mkdir(loc)
	except:
		pass

	links = save_pages("http://www.freebasics.com",loc + "/frb")

	for l in links:
		if l[0] == '/' or l[-2] == "=":
			continue	
		if not ("253471785031826" in l): # look for only learnbasics 
			continue
		save_service(l,loc+"/learnbasics") 

def get_all_links_in_response(links,response):
	soup = BeautifulSoup(response,"html5lib")

	for a in soup.find_all("a"):
		href = a.get("href")
		if href is None or href is "#" or href.endswith(")") or href.endswith(";") or href.startswith("#"):
			continue
		links.add(href)
	return links

def save_service(url,dest):
	global cj	
	http500list = set()
	br = mechanize.Browser()
	br.set_handle_equiv(True)
	br.set_handle_refresh(False)
	br.set_handle_redirect(True)
	br.set_handle_referer(True)
	br.set_handle_robots(False)
	br.set_cookiejar(cj)
	br.addheaders = [('User-agent', "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19 Amreesh")]
	write_log(url)
	try:
		os.mkdir(dest)
	except Exception:
		pass
	#br.clear_history()
	response = br.open(url)
	br.select_form(nr=0)
	#theAction = br.action
	#for cookie in cj:
	#	if("fbs_userid" in cookie.name):
	#		fb_userid=cookie.value
	#		break
	#headers = {'User-agent': 'Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19 Amreesh',u'Referer': theAction, u'content-type': u'application/x-www-form-urlencoded',u'accept': None, u'accept-encoding': u'identity', u'connection': u'close'}
	#data={'serviceid': '253471785031826', 'confirmed': '1', 'userid': fb_userid, 'submit': 'Add'}	
	#res=requests.post(theAction,verify=False,cookies=cj,headers=headers, data=data)	
	#get_all_links_in_response(http500list,res.text)
  	#print http500list	
	try:
		br.select_form(nr=0)	
		res = br.submit("submit")	
	except mechanize.HTTPError, e:
    		if int(e.code) == 500:
			br.back()	
			br.select_form(nr=0)	
			res = br.submit("submit")	
    		else:
        		raise e	
	service1 = res.read()
	page_name = os.path.join(dest, "service1.html")
	with open(page_name, "wb") as fid:
		fid.write(service1)
        for link in br.links():
		if("image" in link.url):
			break
	for i in range(0, 10): 	
		br.clear_history()	
		request = br.click_link(link)
		res = br.open(request)
		service2 = res.read()
		page_name = os.path.join(dest, "service2_"+str(i)+".html")
		with open(page_name, "wb") as fid:
			fid.write(service2)

def save_pages(url,dest):
	global cj	
	languagelist=set()	
	servicelist=set()
	br = mechanize.Browser()
	br.set_handle_equiv(True)
	br.set_handle_redirect(True)
	br.set_handle_referer(True)
	br.set_handle_robots(False)
	br.set_cookiejar(cj)
	br.addheaders = [('User-agent', "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19")]
	try:
		os.mkdir(dest)
	except Exception:
		pass
	
	pagelang=br.open(url)
	get_all_links_in_response(languagelist,pagelang)
	for l in languagelist:
		if("set_locale=en_US" in l):
			url=l
			break
	br.open(url)	
	fc = 0
	for form in br.forms():
   		if str(form.attrs['id']) == 'accept_btn':
       			break
		fc = fc+1
	br.select_form(nr=fc)
	res = br.submit()
	page0 = res.read()
	get_all_links_in_response(servicelist,page0)
	page_name = os.path.join(dest,"page0.html")
	with open(page_name,"wb") as fid:
		fid.write(page0)
	
	for link in br.links():
		if("/searchservices" in link.url):
			break
	request = br.click_link(link)
	res = br.open(request)
	page1 = res.read()
	get_all_links_in_response(servicelist,page1)
	page_name = os.path.join(dest,"page1.html")
	with open(page_name,"wb") as fid:
		fid.write(page1)

	br.select_form(nr=1)
	res = br.submit("next")
	page2 = res.read()
	get_all_links_in_response(servicelist, page2)
	page_name = os.path.join(dest, "page2.html")
	with open(page_name, "wb") as fid:
		fid.write(page2)

	br.select_form(nr=1)
	res = br.submit("next")
	page3 = res.read()
	get_all_links_in_response(servicelist, page3)
	page_name = os.path.join(dest, "page3.html")
	with open(page_name, "wb") as fid:
		fid.write(page3)

	br.select_form(nr=1)
	res = br.submit("next")
	page4 = res.read()
	get_all_links_in_response(servicelist, page4)
	page_name = os.path.join(dest, "page4.html")
	with open(page_name, "wb") as fid:
		fid.write(page4)
	
	br.select_form(nr=1)
	res = br.submit("next")
	page5 = res.read()
	get_all_links_in_response(servicelist, page5)
	page_name = os.path.join(dest, "page5.html")
	with open(page_name, "wb") as fid:
		fid.write(page5)
	return servicelist

def write_log(s):
	with open("log.txt","a") as fid:
		fid.write(s)
		fid.write("\n")

def write_log(s):
	with open("log.txt","a") as fid:
		fid.write(s)
		fid.write("\n")

def finish():
	print "Later..."

if __name__ == "__main__":
	main()

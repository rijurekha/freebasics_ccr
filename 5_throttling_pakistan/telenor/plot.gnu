jpeg_type=1
eps_type=2
png_type=3

op_type=eps_type

if(op_type == eps_type) \
        set term postscript eps color size 3,2
if(op_type == jpeg_type) \
        set term jpeg
if(op_type == png_type) \
        set term pngcairo size 400, 400
set xlabel "Throughput in Kbps"
set ylabel "Cumulative % of reqeusts"
set key below
set xrange[0:600]
set output 'telenor.eps'
plot 'frb_server.txt'  using ($1/1000):2 title 'FRB server' with lines lw 5 lt 2 lc 1,\
'nfrb_server.txt'  using ($1/1000):2 title 'NFRB server' with lines lw 5 lt 2 lc 3,\
'frb_client.txt'  using ($1/1000):2 title 'FRB client' with lines lw 5 lt 1 lc 1,\
'nfrb_client.txt'  using ($1/1000):2 title 'NFRB client' with lines lw 5 lt 1 lc 3 

# README #

### What is this repository for? ###
This contains the crawler frb_crawl.py to fetch all HTMLs over Free Basics for network path experiments using ec2 servers. The learnbasics server is hosted in turn at Virginia, Sao Paolo, Mumbai, Tokyo, Sydney.

This crawler runs on a laptop, tethered over Wi-Fi to a mobile phone with Free Basics SIM Card, acting as Wi-Fi hotspot. The landing pages are different when the URL http://www.freebasics.com is accessed directly from a laptop, vs. through Free Basics. 
![Alt text](addservice.png?raw=true "Adding a new service in Free Basics")

The above figure shows an example of Free Basics access and addition of a service. The script automates this process. It opens a mechanized browser, accesses the Free Basics URL, goes through all the service lists. It clicks on the Learn Basics service (ID 253471785031826) and adds it. It then opens the learnbasics home page, and recursively fetches all HTML files.

![Alt text](learnbasics1.png?raw=true "Learn Basics listed in Free Basics")
![Alt text](learnbasics2.png?raw=true "Learn Basics landing page")
![Alt text](learnbasics3.png?raw=true "Learn Basics subsequent pages which are crawled")

The above figues show Learn Basics listed as a Free Basics service, its landing page and subsequent pages which the script crawls. Since Free Bascis SIM connection is needed to get these pages, the script will crash when called without the Free Basics SIM. The landing page in that case won't match the script's expectation.

mitmproxy captures the raw requests/responses on the client side. tcpdump captures the traces on the server side, from which these requests are filtered using the string "Amreesh" in user agents. 

### Python dependency ###
mechanize
 
### File description ###
The sub-folders contain the mitmproxy and tcpdump data and processed files for Pakistan and SA.

### How to run ###
$python frb_crawl.py

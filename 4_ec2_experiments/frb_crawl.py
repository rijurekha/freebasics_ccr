#Riju
# This is the crawler to fetch all HTMLs over Free Basics for network path experiments using ec2 servers.
# The learnbasics server is hosted in turn at Virginia, Sao Paolo, Mumbai, Tokyo, Sydney.
# This crawler runs on a laptop, tethered over Wi-Fi to a mobile phone with Free Basics SIM Card, acting as Wi-Fi hotspot.
# It opens a mechanized browser, accesses the Free Basics URL, goes through all the service lists.
# It clicks on the Learn Basics service (ID 253471785031826) and adds it.
# It then opens the learnbasics home page, and recursively fetches all HTML files.
# mitmproxy captures the raw requests/responses on the client side.
# tcpdump captures the traces on the server side, from which these requests are filtered using the string "Amreesh" in user agents

from bs4 import BeautifulSoup
import urllib2
import urllib
import time
import os
import random
import ssl
from cookielib import CookieJar
import mechanize
import re

cj = CookieJar()
max_depth = 4
learnbasicscontentlist = set()

def main():
	if hasattr(ssl, '_create_unverified_context'):
		ssl._create_default_https_context = ssl._create_unverified_context

	try:
		os.remove("log.txt")
	except OSError:
		pass

	expmnt_time = time.strftime("%c")
	expmnt_time = expmnt_time.replace (" ", "_")
	write_log(expmnt_time)
	loc = "HTMLs_" + str(expmnt_time)

	try:
		os.mkdir(loc)
	except:
		pass

	links = save_pages("http://www.freebasics.com",loc + "/frb")

	for l in links:
		#print "Saving the link ...."
		if l[0] == '/' or l[-2] == "=":
			continue	
		if not ("253471785031826" in l): # look for only learnbasics 
			continue
		#print l
		save_service(l,loc+"/learnbasics") 

def get_all_links_in_response(links,response):
	soup = BeautifulSoup(response,"html5lib")

	for a in soup.find_all("a"):
		href = a.get("href")
		if href is None or href is "#" or href.endswith(")") or href.endswith(";") or href.startswith("#"):
			continue
		#print href
		links.add(href)
	return links

def save_service(url,dest):
	global cj	
	br = mechanize.Browser()
	br.set_handle_equiv(True)
	br.set_handle_refresh(False)
	br.set_handle_redirect(True)
	br.set_handle_referer(True)
	br.set_handle_robots(False)
	br.set_cookiejar(cj)
	br.addheaders = [('User-agent', "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19 Amreesh")]
	write_log(url)
	try:
		os.mkdir(dest)
	except Exception:
		pass
	br.clear_history()	
	response = br.open(url)
	br.select_form(nr=0)
	res = br.submit("submit")
	service1 = res.read()
	page_name = os.path.join(dest, "service1.html")
	with open(page_name, "wb") as fid:
		fid.write(service1)
        for link in br.links():
		if("available-courses" in link.url):
			break
	br.clear_history()	
	request = br.click_link(link)
	res = br.open(request)
	service2 = res.read()
	page_name = os.path.join(dest, "service2.html")
	with open(page_name, "wb") as fid:
		fid.write(service2)
	contentlist=[]
	chapterlist=[]
	for link in br.links():
		if("iorg_service_id_internal" in link.url):
			if("index.html" not in link.url):
				#print link.url
				contentlist.append(link)
	count=2
	for link in contentlist:
		count=count+1
		br.clear_history()	
		request = br.click_link(link)
		res = br.open(request)
		service = res.read()
		page_name = os.path.join(dest, "service"+str(count)+".html")
		with open(page_name, "wb") as fid:
			fid.write(service)
		for chapterlink in br.links():
			if("iorg_service_id_internal" in chapterlink.url):
				if("index.html" not in chapterlink.url):
					#print chapterlink.url
					chapterlist.append(chapterlink)
	for link in chapterlist:
		if("php?" in link.url):
			br.clear_history()	
			request = br.click_link(link)
			res = br.open(request)
			soup = BeautifulSoup(res)
			image_tags = soup.findAll('img')
			for image in image_tags:
    				filename = image['src']
				if("learnbasics" in filename):
					filename = filename.split('/')[6]
					filename = filename.split('?')[0]
					print filename
					br.clear_history()	
    					data = br.open(image['src']).read()
    					br.back()
    					filename = os.path.join(dest, filename)
   					save = open(filename, 'wb')
    					save.write(data)
    					save.close()		
	
def save_pages(url,dest):
	global cj	
	servicelist=set()
	br = mechanize.Browser()
	br.set_handle_equiv(True)
	br.set_handle_redirect(True)
	br.set_handle_referer(True)
	br.set_handle_robots(False)
	br.set_cookiejar(cj)
	br.addheaders = [('User-agent', "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19")]
	try:
		os.mkdir(dest)
	except Exception:
		pass
	
	br.open(url)
	fc = 0
	for form in br.forms():
   		if str(form.attrs['id']) == 'accept_btn':
       			break
		fc = fc+1
	br.select_form(nr=fc)
	res = br.submit()
	page0 = res.read()
	get_all_links_in_response(servicelist,page0)
	page_name = os.path.join(dest,"page0.html")
	with open(page_name,"wb") as fid:
		fid.write(page0)
	
	for link in br.links():
		#print link.url
		if("/searchservices" in link.url):
			break
	request = br.click_link(link)
	res = br.open(request)
	page1 = res.read()
	get_all_links_in_response(servicelist,page1)
	page_name = os.path.join(dest,"page1.html")
	with open(page_name,"wb") as fid:
		fid.write(page1)
	#print len(servicelist)

	br.select_form(nr=1)
	res = br.submit("next")
	page2 = res.read()
	get_all_links_in_response(servicelist, page2)
	page_name = os.path.join(dest, "page2.html")
	with open(page_name, "wb") as fid:
		fid.write(page2)
	#print len(servicelist)

	br.select_form(nr=1)
	res = br.submit("next")
	page3 = res.read()
	get_all_links_in_response(servicelist, page3)
	page_name = os.path.join(dest, "page3.html")
	with open(page_name, "wb") as fid:
		fid.write(page3)
	#print len(servicelist)

	br.select_form(nr=1)
	res = br.submit("next")
	page4 = res.read()
	get_all_links_in_response(servicelist, page4)
	page_name = os.path.join(dest, "page4.html")
	with open(page_name, "wb") as fid:
		fid.write(page4)
	#print len(servicelist)
	return servicelist

def write_log(s):
	with open("log.txt","a") as fid:
		fid.write(s)
		fid.write("\n")

def write_log(s):
	with open("log.txt","a") as fid:
		fid.write(s)
		fid.write("\n")

def finish():
	print "Later..."

if __name__ == "__main__":
	main()

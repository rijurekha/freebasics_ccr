#!/bin/bash
while read filename;do
	count=`wc -l ${filename}.txt`
	while read ipblock;do
		ipcount=`grep -c $ipblock ${filename}.txt`
		percentage=`echo ${ipcount} ${count} | awk '{print $1*100/$2}'`
		echo $filename $ipblock $percentage
	done < ipblocks.txt
done < filelist
	

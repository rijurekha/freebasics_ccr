# README #

### What is this repository for? ###
This contains the server side tcpdumps for Pakistan, with the server at five different cities. The server side Facebook proxy addresses are recorded in these tcpdump files, which are extracted using the scripts described below. 

### Python dependency ###
pyshark
 
### File description ###
* The tcpdump files are of the format date_cityname.pcap. filelist contains a list of the pcap filenames without the .pcap extension.

* shark.py takes the date_cityname.pcap files and produces corresponding date_cityname_frb.txt files. The format of the date_cityname_frb.txt files with an example from 06112016_virginia_frb.txt is shown below.
```
stream,Facebook server-side proxy IP address, Facebook server-side proxy port, learnbasics server ip, learnbasics server port, requested uri, user-agent

0,66.220.152.164,17564,172.31.51.83,80,/wp-content/themes/twentyfourteen/style.css?ver=4.5.2,[Mozilla/5.0 (BlackBerry; U; BlackBerry 9320; id) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.398 Mobile Safari/534.11+]
```
* ips.sh takes the date_cityname_frb.txt files and produces corresponding date_cityname_ips.txt files. The date_cityname_ips.txt files contain the Facebook server-side proxy IP addresses for the requests that contain "Amreesh" in the user-agent (this user-agent indicates that the request is from our client in Pakistan).

* ipblocks.txt is created using the following bash command. It contains the unique values of the first three cotects of the Facebook server-side proxy addresses.
```bash
cat *_ips.txt | awk -F"." '{print $1"."$2"."$3}' | sort | uniq
```
* ipblocks.sh prints the percentage of each Facebook proxy server-side IP address block for each server location. Values for server at Virginia are as follows.
```
06112016_virginia 31.13.102 0

06112016_virginia 31.13.110 33.3333

06112016_virginia 31.13.113 66.6667

06112016_virginia 31.13.114 0

06112016_virginia 173.252.88 0

06112016_virginia 173.252.90 0

06112016_virginia 66.220.146 0

06112016_virginia 66.220.152 0

06112016_virginia 66.220.156 0

06112016_virginia 66.220.158 0

06112016_virginia 173.252.120 0

06112016_virginia 173.252.123 0

06112016_virginia 173.252.124 0
```
### How to run ###
$python shark.py

$./ips.sh

$./ipblocks.sh

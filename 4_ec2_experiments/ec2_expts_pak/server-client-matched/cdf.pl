#!/usr/bin/perl

use lib ".";
use kv;
use util;

# Program to compute the CDF of a given set of values in an input file

sub usage {
  print STDERR "Usage: $0 <cdf-step> <input-file> <output-file>\n";
} # End usage

($#ARGV == 2) || do { usage(); exit(1); };

my $cdf_step = shift;
my $input_file = shift;
my $output_file = shift;

# Read the input file
print STDERR "Reading the input file...";
open(INPUT, "<$input_file") || die "Can't open $input_file $!\n";
my @input_arr;
my $lc = 0;
while (my $line=<INPUT>) {
  $lc++;
  chop($line);
  $input_arr[$lc-1] = $line;
}
close(INPUT);
print STDERR "done.\n";

# Compute CDF
print STDERR "Computing CDF...";
my @cdf_arr;
util->compute_cdf($cdf_step, \@input_arr, \@cdf_arr);
print STDERR "done.\n";

# Print CDF to output file
print STDERR "Writing the output file...";
open(OUTPUT, ">$output_file") || die "Can't open $output_file $!\n";
my $num_cdf_entries = scalar(@cdf_arr);
for (my $i = 0; $i < $num_cdf_entries; $i++) {
    print OUTPUT "$cdf_arr[$i]\n";
}
close(OUTPUT);
print STDERR "done.\n";

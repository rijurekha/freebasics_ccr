#!/bin/bash
for city in {virginia,saopaolo,mumbai,tokyo,sydney};do
	echo $city
	rm ${city}*txt
	grep "Amreesh" ../serverside/*_${city}_frb.txt | grep html | awk -F"," '{print $2,$6}' | sort -k2,2 > ${city}_server.txt
	cat ../clientside/*_${city}/*mitm.txt | grep html| grep -v leaving | awk '{print $2,$3}' | sort -k2,2 | uniq > ${city}_client.txt
	paste ${city}_server.txt ${city}_client.txt | awk '{print $3,$1,$2,$4}'> ${city}_matched.txt
done

# Perl package with various utilities

package util;
use strict;

# The constructor
sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self  = {};
  # Nothing to do
  bless ($self, $class);
  return $self;
} # End constructor

# Return the directory from the given file-path (upto the last '/' but
# not including it); Returns "/" for "/xyz" as argument; returns "" if
# argument does not have any "/"
sub get_dir_from_path($) {
  my $self = shift; # Dummy
  my $path = shift;

  my $ind = rindex($path, "/");
  if ($ind == -1) {
    return "";
  }
  if ($ind == 0) {
    return "/";
  }
  return substr($path, 0, $ind);
} # End get_dir_from_path

# Return the index of the maximum in a given array; -1 if array is
# empty
sub max_index_in_array($) {
  my $self = shift; # Dummy
  my $in_ref = shift; # Ref to input array
  my $len = scalar(@{$in_ref});
  ($len > 0) || return -1;
  my $max_index = 0;
  for (my $i = 0; $i < $len; $i++) {
    if ($in_ref->[$i] > $in_ref->[$max_index]) {
      $max_index = $i;
    }
  }
  return $max_index;
} # End max_index_in_array

# Return the index of the minimum in a given array; -1 if array is
# empty
sub min_index_in_array($) {
  my $self = shift; # Dummy
  my $in_ref = shift; # Ref to input array
  my $len = scalar(@{$in_ref});
  ($len > 0) || return -1;
  my $min_index = 0;
  for (my $i = 0; $i < $len; $i++) {
    if ($in_ref->[$i] < $in_ref->[$min_index]) {
      $min_index = $i;
    }
  }
  return $min_index;
} # End min_index_in_array

# Compute the CDF of a given array of values.  Takes as input, the
# step to be used for computing the CDF.  Return CDF in the given
# array ref, as an array of "value\tpercentage" strings.
sub compute_cdf($$$) {
  my $self = shift; # Implicit argument
  my $step = shift;
  my $in_ref = shift; # Ref to input array
  my $out_ref = shift; # Ref to output array

  my $count = scalar(@{$in_ref});

  if($count == 0) {
    $out_ref->[0] = "0\t100";
    $out_ref->[1] = "1\t100";
    return;
  }

  my $max_index = $self->max_index_in_array($in_ref);
  my $min_index = $self->min_index_in_array($in_ref);
  my $max = $in_ref->[$max_index];
  my $min = $in_ref->[$min_index];

  my $out_ind = 0;
  for(my $val = $min; $val <= ($max+$step); $val += $step) {
    my $freq = 0;
    for(my $j = 0; $j < $count; $j++) {
      if($in_ref->[$j] <= $val) { $freq++; }
    }
    my $perc = 100.0*$freq/$count;
    $out_ref->[$out_ind] = "$val\t$perc";
    $out_ind++;
  }
} # End compute_cdf

1; # So that use succeeds

jpeg_type=1
eps_type=2
png_type=3

op_type=eps_type

if(op_type == eps_type) \
        set terminal postscript eps color size 3,2
if(op_type == jpeg_type) \
        set term jpeg
if(op_type == png_type) \
        set term png size 500,300
set key below
set yrange[0:100]
set ytic 20
set ylabel "CDF"
set xlabel "Ping delays (msecs)"

set output 'pak.eps'
plot 'virginia_times_out' using ($1):($2) title 'Virginia FRB' with lines  lw 6 lt 1 lc 1,\
     'saopaolo_times_out' using ($1):($2) title 'Sao Paolo FRB' with lines  lw 6 lt 1 lc 4,\
     'mumbai_times_out' using ($1):($2) title 'Mumbai FRB' with lines  lw 6 lt 1 lc 3,\
     'tokyo_times_out' using ($1):($2) title 'Tokyo FRB' with lines  lw 6 lt 1 lc rgb '#00ad88',\
     'sydney_times_out' using ($1):($2) title 'Sydney FRB' with lines  lw 6 lt 1 lc 7,\
     'virginia_direct' using ($1):($2) title 'Virginia direct' with lines  lw 6 lt 2 lc 1,\
     'saopaolo_direct' using ($1):($2) title 'Sao Paolo direct' with lines  lw 6 lt 2 lc 4,\
     'mumbai_direct' using ($1):($2) title 'Mumbai direct' with lines  lw 6 lt 2 lc 3,\
     'tokyo_direct' using ($1):($2) title 'Tokyo direct' with lines  lw 6 lt 2 lc rgb '#00ad88',\
     'sydney_direct' using ($1):($2) title 'Sydney direct' with lines  lw 6 lt 2 lc 7

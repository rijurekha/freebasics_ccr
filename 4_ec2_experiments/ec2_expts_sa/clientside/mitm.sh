#!/bin/bash
ls *_out | sed 's/_out//g' > list
rm ${1}*.txt *_mitm.txt
while read filename;do
	python read_dumpfile.py ${filename}_out >> ${filename}_mitm.txt
	cat  ${filename}_mitm.txt | awk '{print $2}' >> $1.txt 
done < list
cat $1.txt | sort -n | uniq > temp
mv temp ${1}_uniq.txt

while read ip;do
	count=`grep -c $ip $1.txt`
	total=`wc -l $1.txt | awk '{print $1}'`
	percent=`echo $count $total| awk '{print $1*100/$2}'`
	echo $count $total $ip $percent
done < ${1}_uniq.txt

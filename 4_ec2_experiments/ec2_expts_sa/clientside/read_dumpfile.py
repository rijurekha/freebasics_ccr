#!/usr/bin/env python
#
# Simple script showing how to read a mitmproxy dump file
#

from mitmproxy import flow
import pprint
import sys

with open(sys.argv[1], "rb") as logfile:
    freader = flow.FlowReader(logfile)
    pp = pprint.PrettyPrinter(indent=4)
    try:
        for f in freader.stream():
            #print(f)
            #print(f.request.host, f.client_conn.address.address,f.response)
		t1 = f.client_conn.timestamp_start
		t2 = f.server_conn.timestamp_start
		t3 = f.server_conn.timestamp_tcp_setup
		t4 = f.server_conn.timestamp_ssl_setup
		t5 = f.client_conn.timestamp_ssl_setup
		t6 = f.request.timestamp_start
		t7 = f.request.timestamp_end
		t8 = f.response.timestamp_start
		t9 = f.response.timestamp_end
		response_size =  len(str(f.response).split(" "))
		size = str(f.response).split(" ")[response_size-1].split("k")[0]
		address = str(f.client_conn.address.address).split("'")[1]
		#print (t1, t2, t3, t4, t5, t6, t7, t8, t9)	
            	if(str(t1) != "None" and str(t2) != "None" and str(t3)!= "None" and str(t4) != "None" and str(t5) != "None" and str(t6) != "None" and str(t7) != "None" and str(t8) != "None" and str(t9) != "None"):
	  		print address ,f.request.host, f.request.path, size, (t2-t1)*1000, (t3-t2)*1000, (t4-t3)*1000, (t5-t4)*1000, (t7-t6)*1000, (t9-t8)*1000 
	  	#print address ,f.request.host, f.request.path, size, (t2-t1)*1000, (t3-t2)*1000, "None", "None", (t7-t6)*1000, (t9-t8)*1000 
            #print(f.request.timestamp_start,f.request.server_conn.timestamp_ssl_setup, f.request.server_conn.timestamp_tcp_setup,  f.request.timestamp_end)
            #pp.pprint(f.get_state())
            #print("")
    except:
        print("Flow file corrupted")

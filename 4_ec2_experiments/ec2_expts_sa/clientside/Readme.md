# README #

### What is this repository for? ###
This contains the client side mitmproxy dumps for SA, with the server at five different cities. The client-side Facebook proxy addresses are recorded in these mitm files, which are extracted using the scripts described below. 

### Python dependency ###
mitmproxy
 
### File description ###
1. There are 5 sub-folders of the form date_cityname, containg the mitmproxy dumps of the form frb1_out, frb2_out ...

2. mitm.sh calls internally read_dumpfile.py to read the mitm dump files into text files, extract Facebook proxy IP addresses from the text files and count the percentage occurence of each proxy IP. mitm.sh and read_dumpfile.py need to be copied into each dat_cityname sub-folder and run.  

3. list, ${city}.txt and ${city}_unique.txt are intermediate files created. list has the name of the mitm dumps, ${city}.txt has all the proxy IP addresses recorded in the mitm dumps and ${city}_unique.txt has the unique IP addresses. The output is printed on the console, showing number of occurences of a particular Facebook proxy IP, total number of occurences, Facebook proxy IP address and percentage of occurence of that IP. An example output for 02112016_virginia is shown below. 

4364 4364 31.13.90.37 100

### How to run ###
$./mitm.sh cityname

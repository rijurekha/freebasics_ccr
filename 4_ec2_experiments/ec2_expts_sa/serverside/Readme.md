# README #

### What is this repository for? ###
This contains the server side tcpdumps for SA, with the server at five different cities. The server side Facebook proxy addresses are recorded in these tcpdump files, which are extracted using the scripts described below. 

### Python dependency ###
pyshark
 
### File description ###
* The tcpdump files are of the format date_cityname.pcap. filelist contains a list of the pcap filenames without the .pcap extension.

* shark.py takes the date_cityname.pcap files and produces corresponding date_cityname_frb.txt files. The format of the date_cityname_frb.txt files with an example from 02112016_virginia_frb.txt is shown below.
```
stream,Facebook server-side proxy IP address, Facebook server-side proxy port, learnbasics server ip, learnbasics server port, requested uri, user-agent

13,66.220.152.173,23638,172.31.51.83,80,/,[Mozilla/5.0 (Linux; Android 4.4.2; SM-G800H Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36[FBAN/InternetOrgApp; FBAV/5.0;]]
```
* ips.sh takes the date_cityname_frb.txt files and produces corresponding date_cityname_ips.txt files. The date_cityname_ips.txt files contain the Facebook server-side proxy IP addresses for the requests that contain "Amreesh" in the user-agent (this user-agent indicates that the request is from our client in Pakistan).

* ipblocks.txt is created using the following bash command. It contains the unique values of the first three cotects of the Facebook server-side proxy addresses.
```bash
cat *_ips.txt | awk -F"." '{print $1"."$2"."$3}' | sort | uniq
```
* ipblocks.sh prints the percentage of each Facebook proxy server-side IP address block for each server location. Values for server at Virginia are as follows.
```
02112016_virginia_ips 31.13.102 0

02112016_virginia_ips 31.13.110 22.7273

02112016_virginia_ips 31.13.113 42.0455

02112016_virginia_ips 31.13.114 6.81818

02112016_virginia_ips 69.63.188 28.4091

02112016_virginia_ips 173.252.88 0

02112016_virginia_ips 173.252.90 0
```
### How to run ###
$python shark.py

$./ips.sh

$./ipblocks.sh

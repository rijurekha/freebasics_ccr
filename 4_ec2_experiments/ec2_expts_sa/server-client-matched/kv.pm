# Perl package to deal with key-value pairs

package kv;
use strict;

# The constructor
sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self  = {};
  $self->{PAIRS} = ();
  bless ($self, $class);
  return $self;
} # End new

# Read a bunch of key-value pairs onto the given hash, from the given
# string.  Format of the string is assumed to be "k1=v1,k2=v2,k3=v3..."
sub read_kv($) {
  my $self = shift;
  my $href = \%{$self->{PAIRS}};
  %{$href} = (); # empty to begin with
  my $s = shift;
  my @fields = split(/\,/, $s);
  foreach my $field (@fields) {
    my ($key, $val) = split(/=/, $field);
    $href->{$key} = $val;
  }
} # End read_kv()

# Add a key-value pair
sub add_kv($$) {
  my $self = shift;
  my $key = shift;
  my $val = shift;
  my $href = \%{$self->{PAIRS}};
  $href->{$key} = $val;
} # End add_kv()

# Get the value corresponding to a given key, undef on failure
sub get_value($) {
  my $self = shift;
  my $key = shift;
  my $href = \%{$self->{PAIRS}};
  return $href->{$key};
} # End get_value()

# Is a value corresponding to the given key present?
sub keyExists($) {
  my $self = shift;
  my $key = shift;
  my $href = \%{$self->{PAIRS}};
  return defined($href->{$key});
} # End keyExists()

# Return the key-value pairs as a string
sub toStr() {
  my $self = shift;
  my $href = \%{$self->{PAIRS}};
  my $s = "";
  my $prefix = "";
  foreach my $key (keys %{$href}) {
    $s .= $prefix.$key."=".$href->{$key};
    $prefix = ",";
  }
  return $s;
} # End toStr()

1; # so that use succeeds

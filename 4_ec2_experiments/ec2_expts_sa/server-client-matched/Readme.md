# README #

### What is this repository for? ###
Matching HTML requests from mitmdump collected at clients and tcpdump simultaneously collected at learnbasics server. Extract the Facebook client-side and server-side proxy IPs from the matched requests. Get the ping delays between (a) the client and the Facebook client-side proxy and (b) the Facebook server-side proxy and the learnbasics server. Compare the combined ping delays (a)+(b) to the direct delay between the client and the learnbasics server. 

### File description ###
* prox1_sa.txt contain ping times from the SA client to the Facebook client side proxy.

* ${city}_sa.txt contain ping times from the learnbasics server at $city to the Facebook server-side proxy, when the mobile client was in SA.

* matched.sh produces ${city}_matched.txt that contain matched requests from client side mitmdumps and server side tcpdumps. Format and an example from virginia_matched.txt are as follows.
```
Facebook client-side proxy IP, Facebook server-side proxy IP, requested URI at learnbasics server, requested URI at Pakistan client
31.13.90.37 31.13.110.127 /available-courses.html /available-courses.html?iorg_service_id_internal=253471785031826%3BAfofNZIfdUmBDf16
```
${city}\_server.txt and ${city}\_client.txt are intermediate files produced,  using files \*\_${city}_frb.txt from [../serverside](../serverside) sub-folder and files \*\_${city}/*mitm.txt from [../clientside](../clientside) respectively. ${city}\_server.txt and ${city}\_client.txt are then matched to produce ${city}\_matched.txt.

* times.sh takes the matched requests, gets the Facebook client-side proxy IP and Facebook server-side proxy IP for a particular request, and sums the ping times from client to that client side proxy from prox1_pak.txt and server side proxy to server from ${city}_pak.txt. ${city}_times.txt are intermediate files containing the two IP addresses and two ping times. The summed time is written to ${city}_times_in. 

* times.sh internally calls cdf.pl, which internally calls util.pm and kv.pm to compute the cdf from ${city}_times_in and produce output ${city}_times_out. ${city}_direct contains the cdf output for ping measurements directly from the Pakistan client to the learnbasics server at ${city}. cdf.gnu plots these cdf output files for comparison of delays between the direct path and proxy-based path.

### How to run ###
$./matched.sh

$./times.sh

$gnuplot cdf.gnu

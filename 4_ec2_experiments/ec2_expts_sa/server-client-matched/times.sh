#!/bin/bash
for city in {virginia,saopaolo,mumbai,tokyo,sydney};do
	rm ${city}_times.txt
	while read line;do
		proxy1=`echo $line | awk '{print $1}'`
		proxy2=`echo $line | awk '{print $2}'`
		time1=`grep ${proxy1} proxy1_sa.txt | awk '{print $2}'`
		time2=`grep ${proxy2} ${city}_sa.txt | awk '{print $2}'`
		echo $proxy1 $proxy2 $time1 $time2 >> ${city}_times.txt 
	done < ${city}_matched.txt
	cat ${city}_times.txt | awk '{print $3+$4}' > ${city}_times_in
	./cdf.pl 1 ${city}_times_in ${city}_times_out
done 
